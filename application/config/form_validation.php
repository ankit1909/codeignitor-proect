<?php

$config=[
        'add_articles_rules'=>[
                            [
                                'field' => 'title',
                                'label' => 'article title',
                                'rules' => 'required|alpha'
                            ],
                            [
                                'field' => 'body',
                                'label' => 'article body',
                                'rules' => 'required|alpha'
                            ]   
                            ],  
                        ];

?>