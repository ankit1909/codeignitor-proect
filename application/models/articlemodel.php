<?php
class Articlemodel extends CI_Model {
    public function articles_list($limit,$offset)
   {
         $user_id = $this->session->userdata('user_id');
         $query = $this->db
                       ->select()
                       ->from('articles')
                       ->where(['user_id'=> $user_id])
                       ->limit($limit,$offset)
                       ->get();
          return $query->result();     
   }

   public function num_rows(){
   $user_id = $this->session->userdata('user_id');
   $query = $this->db
                  ->select()
                  ->from('articles')
                  ->where(['user_id'=> $user_id])
                  ->get();
     return $query->num_rows();
   }

    public function add_article($array)
    {
      return $this->db->INSERT('articles', $array);
    }

    public function search_article($id)
    {    
       $r = $this->db->SELECT(['id','title','body'])
                     ->WHERE('id',$id)
                     ->get('articles');
            return $r->row();
    }

    public function update_article($id, $article)
    {
      return $this->db->WHERE('id',$id)
                      ->UPDATE('articles',$article);
    }

    public function delete_article($id)
    {
       return $this->db->delete('articles',['id'=>$id]);
    }

  
}
?>