<?php
class Login extends MY_Controller
{
    public function index()
    {
        $this->load->helper('form');
        $this->load->view('admin/admin_login'); 
    }

    public function admin_login()
    {
        $this->form_validation->set_rules('username','User Name','required');
        $this->form_validation->set_rules('password','password','required');

        $this->form_validation->set_error_delimiters("<p class='text-danger'>","</p>");

        if( $this->form_validation->run() ){

                $username = $this->input->post('username');
                $password = $this->input->post('password');
                $this->load->model('loginmodel');
                $login_id = $this->loginmodel->valid_login($username,$password);
                if($login_id){
                    $this->load->library('session');

                    $this->session->set_userdata('user_id', $login_id);
                    //valid, login user
                    
                    return redirect('Admin/dashboard');
                }else{
                    //failed login
                    $this->session->set_flashdata('login_failed','invalid username/password');
                   return redirect('login');
                }
        }else{
            //failed
            $this->load->view('admin/admin_login'); 
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_id');
        return redirect("Login");
    }
}
?>